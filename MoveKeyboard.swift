//
//  MoveKeyboard.swift
//  ZupTestCaroseul
//
//  Created by Brenno on 20/11/16.
//  Copyright © 2016 Brenno. All rights reserved.
//

import UIKit

class MoveKeyboard {
    static func animateViewMoving (_ view:UIView, up:Bool, moveValue :CGFloat) {
        
        let movementDuration:TimeInterval = 0.3
        
        let movement:CGFloat = ( up ? -moveValue : moveValue)
        
        UIView.beginAnimations("animateView", context: nil)
        
        UIView.setAnimationBeginsFromCurrentState(true)
        
        UIView.setAnimationDuration(movementDuration)
        
        view.frame = view.frame.offsetBy(dx: 0,  dy: movement)
        
        UIView.commitAnimations()
    }
}
