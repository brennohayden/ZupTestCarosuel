### Instalação

Recomenda-se utilizar um gerenciador de dependência [CocoaPods](http://cocoapods.org/) , gerenciamento de dependência flexível e instalação simples.


#### CocoaPods

Instale CocoaPods:

``` bash
$ [sudo] gem install cocoapods
$ pod setup
```
Escolha um diretório e baixe o projeto _Midiacode-IOS_ :
```bash
$ cd /path/to/MyProject
$ git clone https://gitlab.com/brennohayden/ZupTestCarosuel.git
```
Instale o Projeto:

``` bash
$ pod install
```

Abrir o Projeto:

``` bash
$ open ZupTestCarosuel.xcworkspace/
```
