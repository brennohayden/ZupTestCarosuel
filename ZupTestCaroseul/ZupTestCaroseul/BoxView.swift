//
//  BoxView.swift
//  ZupTestCaroseul
//
//  Created by Brenno on 20/11/16.
//  Copyright © 2016 Brenno. All rights reserved.
//

import UIKit

@IBDesignable class BoxView: UIView {

    @IBInspectable var cornerRadius: CGFloat = 10.0
    @IBInspectable var borderColor: UIColor = UIColor.gray
    @IBInspectable var borderWidth: CGFloat = 0.5
    fileprivate var customBackgroundColor = UIColor.white
    
    override var backgroundColor: UIColor?{
        didSet {
            customBackgroundColor = backgroundColor!
            super.backgroundColor = UIColor.clear
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.setup()
    }
    
    
    func setup() {
        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 10.0
        layer.shadowOpacity = 0.5
        super.backgroundColor = UIColor.clear
    }
    

    override func draw(_ rect: CGRect) {
        
        customBackgroundColor.setFill()
        
        UIBezierPath(roundedRect: bounds, cornerRadius: cornerRadius ).fill()
        
        let borderRect = bounds.insetBy(dx: borderWidth/2, dy: borderWidth/2)
        let borderPath = UIBezierPath(roundedRect: borderRect, cornerRadius: cornerRadius - borderWidth/2)
        borderColor.setStroke()
        borderPath.lineWidth = borderWidth
        borderPath.stroke()
        
    }

}
