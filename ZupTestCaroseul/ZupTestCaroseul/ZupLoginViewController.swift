//
//  ZupLoginViewController.swift
//  ZupTestCaroseul
//
//  Created by Brenno on 20/11/16.
//  Copyright © 2016 Brenno. All rights reserved.
//

import UIKit
// FIXME: comparison operators with optionals were removed from the Swift Standard Libary.
// Consider refactoring the code to use the non-optional operators.
fileprivate func < <T : Comparable>(lhs: T?, rhs: T?) -> Bool {
  switch (lhs, rhs) {
  case let (l?, r?):
    return l < r
  case (nil, _?):
    return true
  default:
    return false
  }
}



class ZupLoginViewController: UIViewController {

    
    @IBOutlet weak var tfUsername: UITextField!
    
    @IBOutlet weak var tfPassword: UITextField!
    
    let gradientColor = GradientColor()

    override func viewDidLoad() {
        super.viewDidLoad()

        setup()
        
        tfUsername.delegate = self
        tfUsername.placeholder = "Username"
        tfPassword.delegate = self
        tfPassword.placeholder = "Password"
        
    }

    func setup() {
        
        view.backgroundColor = UIColor.clear
        
        let backgroundLayer = gradientColor.layer
        
        backgroundLayer.frame = view.frame
        
        view.layer.insertSublayer(backgroundLayer, at: 0)
    }
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
        
    }

    //MARK - Button press
    @IBAction func lostPassPressed(_ sender: AnyObject) {
        print("lostPassPressed")
    }
    
    @IBAction func SignInPressed(_ sender: AnyObject) {
        
        if tfUsername.text!.isEmpty || tfPassword.text!.isEmpty {
            
            self.view.endEditing(true)
            
            let alertController = UIAlertController(title: NSLocalizedString("ERROR", comment:""), message:
                NSLocalizedString("EMPTY", comment:""), preferredStyle: UIAlertControllerStyle.alert)
            alertController.addAction(UIAlertAction(title: NSLocalizedString("BACK", comment:""), style: UIAlertActionStyle.default,handler: nil))
            
            self.present(alertController, animated: true, completion: nil)
            
        }else {
            
            if tfPassword.text?.characters.count < 6 {
                
                self.view.endEditing(true)
                
                let alertController = UIAlertController(title: NSLocalizedString("ERROR", comment:""), message:
                    NSLocalizedString("PASS", comment:""), preferredStyle: UIAlertControllerStyle.alert)
                alertController.addAction(UIAlertAction(title: NSLocalizedString("BACK", comment:""), style: UIAlertActionStyle.default,handler: nil))
                
                self.present(alertController, animated: true, completion: nil)
            
            }else {
                
                //next UI
                
                let storyboard = UIStoryboard(name: "Main", bundle: nil)
                
                let vc = storyboard.instantiateViewController(withIdentifier: "ZupCarouselViewController") as! ZupCarouselViewController
                
                self.present(vc, animated: true, completion: nil)
            
            }
        }
    }
}

extension ZupLoginViewController: UITextFieldDelegate {
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        MoveKeyboard.animateViewMoving(view, up: true, moveValue: 100)
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        MoveKeyboard.animateViewMoving(view, up: false, moveValue: 100)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if (textField === tfUsername) {
            
            tfPassword.becomeFirstResponder()
            
        }else if (textField === tfPassword) {
            
            tfPassword.resignFirstResponder()
            
        }
        return true
    }
}
