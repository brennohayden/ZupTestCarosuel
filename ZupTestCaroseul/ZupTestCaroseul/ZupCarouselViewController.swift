//
//  ZupCarouselViewController.swift
//  ZupTestCaroseul
//
//  Created by Brenno on 20/11/16.
//  Copyright © 2016 Brenno. All rights reserved.
//

import UIKit

class ZupCarouselViewController: UIViewController {

    @IBOutlet weak var horizontalCarousel: iCarousel!
    @IBOutlet weak var verticalCarousel: iCarousel!
    @IBOutlet weak var toolbar: UIToolbar!

    var categoriaHorizontalArray = [Categoria]()
    
    let imagesArray = ["ic_delivery", "ic_design", "ic_ecommerce", "ic_social", "ic_delivery"]
    
    var categoryRow = 0
    
    var itemRow = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupCategory()
        
        setupToolbar()

        horizontalCarousel.dataSource = self
        horizontalCarousel.delegate = self
        verticalCarousel.dataSource = self
        verticalCarousel.delegate = self
                
        //horizontal
        horizontalCarousel!.type = .rotary
        
        //vertical
        verticalCarousel!.type = .rotary
        verticalCarousel!.isVertical = true
        
        //setup
        setupGesture()
    }
    
    func setupGesture() {
        
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(ZupCarouselViewController.respondToSwipeGesture(_:)))
        swipeUp.direction = UISwipeGestureRecognizerDirection.up
        self.view.addGestureRecognizer(swipeUp)
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(ZupCarouselViewController.respondToSwipeGesture(_:)))
        swipeDown.direction = UISwipeGestureRecognizerDirection.down
        self.view.addGestureRecognizer(swipeDown)
        
        //horizontal
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(ZupCarouselViewController.respondToSwipeGesture(_:)))
        swipeLeft.direction = UISwipeGestureRecognizerDirection.left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(ZupCarouselViewController.respondToSwipeGesture(_:)))
        swipeRight.direction = UISwipeGestureRecognizerDirection.right
        self.view.addGestureRecognizer(swipeRight)
    }
    
    func setupCategory() {

        addCategory("Categoria 1", image: imagesArray[0], item: "cat1 / item 1", imgItem: imagesArray[0])
        addItem(0, item: "cat1 / item 2", imagem: imagesArray[0])
        addItem(0, item: "cat1 / item 3", imagem: imagesArray[0])
        
        addCategory("Categoria 2", image: imagesArray[1], item: "cat2 / item 1", imgItem: imagesArray[1])
        addItem(1, item: "cat2 / item 2", imagem: imagesArray[1])
        addItem(1, item: "cat2 / item 3", imagem: imagesArray[1])

        addCategory("Categoria 3", image: imagesArray[2], item: "cat3 / item 1", imgItem: imagesArray[2])
        addItem(2, item: "cat3 / item 2", imagem: imagesArray[2])
        addItem(2, item: "cat3 / item 3", imagem: imagesArray[2])
        
        addCategory("Categoria 4", image: imagesArray[3], item: "cat4 / item 1", imgItem: imagesArray[3])
        addItem(3, item: "cat4 / item 2", imagem: imagesArray[3])
        addItem(3, item: "cat4 / item 3", imagem: imagesArray[3])
    
    }
    
    func setupToolbar() {
        let buttonItem = UIBarButtonItem(title: "Option",
                                    style: UIBarButtonItemStyle.plain,
                                    target: self,
                                    action: #selector(ZupCarouselViewController.options))
        toolbar.items?.append(buttonItem)
    }
    
    func options() {
        let alert = UIAlertController(title: "Options", message: "Options", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Add Category", style: .default, handler: { action in
            
            var inputTextField: UITextField?
            let passwordPrompt = UIAlertController(title: NSLocalizedString("CAT_TITLE", comment:""), message: NSLocalizedString("CAT_MSG", comment:""), preferredStyle: UIAlertControllerStyle.alert)
            
            passwordPrompt.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment:""), style: UIAlertActionStyle.default, handler: nil))
            passwordPrompt.addAction(UIAlertAction(title: NSLocalizedString("CONFIRM", comment:""), style: UIAlertActionStyle.default, handler: {
                (action) -> Void in
                
                self.addCategory((inputTextField?.text)!, image: self.imagesArray[0], item: "new title", imgItem: self.imagesArray[0])
                
                self.horizontalCarousel.reloadData()
                
            }))
            passwordPrompt.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "item"
                inputTextField = textField
                
            })
            self.present(passwordPrompt, animated: true, completion: nil)
            
        }))
        
        alert.addAction(UIAlertAction(title: "Add Item", style: .default, handler: { action in
            
            var inputTextField: UITextField?
            let passwordPrompt = UIAlertController(title: NSLocalizedString("ITEM_TITLE", comment:""), message: NSLocalizedString("ITEM_MSG", comment:""), preferredStyle: UIAlertControllerStyle.alert)
            
            passwordPrompt.addAction(UIAlertAction(title: NSLocalizedString("CANCEL", comment:""), style: UIAlertActionStyle.default, handler: nil))
            passwordPrompt.addAction(UIAlertAction(title: NSLocalizedString("CONFIRM", comment:""), style: UIAlertActionStyle.default, handler: {
                (action) -> Void in
                self.addItem(self.categoryRow, item: (inputTextField?.text)!, imagem: self.imagesArray[0])
                self.verticalCarousel.reloadData()
                
            }))
            passwordPrompt.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "item"
                inputTextField = textField
            })
            self.present(passwordPrompt, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Remove Category", style: .default, handler: { action in
            let catIndex = self.horizontalCarousel.currentItemIndex
            self.categoriaHorizontalArray.remove(at: catIndex)
            self.horizontalCarousel.reloadData()
            
        }))
        
        alert.addAction(UIAlertAction(title: "Remove Item", style: .default, handler: { action in
            if self.categoriaHorizontalArray[self.categoryRow].itensCount() > 1 {
                self.categoriaHorizontalArray[self.categoryRow].itens.remove(at: self.itemRow)
                self.verticalCarousel.reloadData()
            }else {
                let alertCategoria = UIAlertController(title: "Options", message: NSLocalizedString("CATEGORIA", comment:""), preferredStyle: .alert)
                alertCategoria.addAction(UIAlertAction(title: NSLocalizedString("YES", comment:""), style: .default, handler: { action in
                    let catIndex = self.horizontalCarousel.currentItemIndex
                    self.categoriaHorizontalArray.remove(at: catIndex)
                    self.horizontalCarousel.reloadData()
                }))
                alertCategoria.addAction(UIAlertAction(title: NSLocalizedString("NO", comment:""), style: .cancel, handler: nil))
                self.present(alertCategoria, animated: true, completion: nil)
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    func respondToSwipeGesture(_ gesture: UIGestureRecognizer) {
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                view.bringSubview(toFront: horizontalCarousel)
                
            case UISwipeGestureRecognizerDirection.left:
                view.bringSubview(toFront: horizontalCarousel)
                
            case UISwipeGestureRecognizerDirection.up:
                view.bringSubview(toFront: verticalCarousel)
                
            case UISwipeGestureRecognizerDirection.down:
                view.bringSubview(toFront: verticalCarousel)
                
            default:
                break
            }
        }
    }
    
    //MARK: - category
    
    func addCategory(_ title: String, image: String, item: String, imgItem: String) {
        
        var category = Categoria(title: title, image: image)
        
        category.itens.append(Item(title: item, image: imgItem))

        categoriaHorizontalArray.append(category)

    }
    
    func addItem(_ index: Int, item: String, imagem: String) {
        
        if index >= 0 {
            
            categoriaHorizontalArray[index].itens.append(Item(title: item, image: imagem))
        }
    }
}

extension ZupCarouselViewController: iCarouselDelegate, iCarouselDataSource {

    func numberOfItems(in carousel: iCarousel) -> Int {
        
        switch carousel {
        case horizontalCarousel:
            return categoriaHorizontalArray.count
        case verticalCarousel:
            if categoriaHorizontalArray.count > 0  {
                return categoriaHorizontalArray[categoryRow].itens.count
            }
                return 0
            
        default: return 0
        }
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
        
        let tempView = BoxView(frame: CGRect(x: 0, y: 0, width: 200, height: 200))
        let itemView = UIImageView(frame: CGRect(x: 50, y: 20, width: tempView.frame.size.width * 0.5, height: tempView.frame.size.height * 0.5))

        if (carousel == horizontalCarousel) {
            itemView.image = UIImage(named: categoriaHorizontalArray[index].image)
            tempView.addSubview(itemView)
            
            let label = UILabel(frame: CGRect(x: 0, y: 90, width: tempView.frame.size.width, height: tempView.frame.size.height * 0.5))
            label.backgroundColor = UIColor.clear
            label.textAlignment = .center
            label.tag = 1
            label.text = categoriaHorizontalArray[index].title
            label.textColor = UIColor.blue
            tempView.addSubview(label)

        }else if carousel == verticalCarousel {
            itemView.image = UIImage(named: categoriaHorizontalArray[categoryRow].itens[index].image)
            tempView.addSubview(itemView)
            
            let label = UILabel(frame: CGRect(x: 0, y: 90, width: tempView.frame.size.width, height: tempView.frame.size.height * 0.5))
            label.backgroundColor = UIColor.clear
            label.textAlignment = .center
            label.tag = 1
            label.text = categoriaHorizontalArray[categoryRow].itens[index].title
            label.textColor = UIColor.blue
            tempView.addSubview(label)
        }
        return tempView
    }
    
    func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
        switch (option) {
        case .wrap:
            return 1
            
        case .spacing:
            return value// * (-1.5)
            
        default:
            return value
        }
    }
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        
        if carousel == horizontalCarousel {
            categoryRow = carousel.currentItemIndex
            verticalCarousel.reloadData()
        }else if carousel == verticalCarousel {
            itemRow = carousel.currentItemIndex
        }
    }
}







