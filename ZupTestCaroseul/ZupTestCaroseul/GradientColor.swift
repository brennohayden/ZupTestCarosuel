//
//  GradientColor.swift
//  ZupTestCaroseul
//
//  Created by Brenno on 20/11/16.
//  Copyright © 2016 Brenno. All rights reserved.
//

import UIKit

class GradientColor {
    
    let colorTop = UIColor(red: 86.0/255.0, green: 50.0/255.0, blue: 86.0/255.0, alpha: 1.0).cgColor
    
    let colorCenter = UIColor(red: 0.0/255.0, green: 0.0/255.0, blue: 0.0/255.0, alpha: 1.0).cgColor
    
    let colorBottom = UIColor(red: 86.0/255.0, green: 50.0/255.0, blue: 86.0/255.0, alpha: 1.0).cgColor
    
    
    let layer: CAGradientLayer
    
    init() {
        
        layer = CAGradientLayer()
        
        layer.colors = [colorCenter, colorTop, colorCenter]
        
        layer.locations = [ 0.0, 1.0]
    }
}
