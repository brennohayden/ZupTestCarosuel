//
//  struct.swift
//  ZupTestCaroseul
//
//  Created by Brenno on 21/11/16.
//  Copyright © 2016 Brenno. All rights reserved.
//

import Foundation

struct Item {
    let title: String
    let image: String
    
    init(title: String, image: String) {
        self.title = title
        self.image = image
    }
}

struct Categoria {
    let title: String
    let image: String
    
    var itens = [Item]()
    
    init(title: String, image: String) {
        self.title = title
        self.image = image
    }
    
    mutating func addItem(_ item: Item){
        itens.append(item)
    }
    
    mutating func removeItem(_ index: Int) {
        itens.remove(at: index)
    }
    
    func itensCount() -> Int {
        return itens.count
    }
}
